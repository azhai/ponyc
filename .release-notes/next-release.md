## Remove `stable` from Docker images

Previously, we were including our very old and very deprecated dependency management tool `stable` in our `ponyc` Docker images. We've stopped.

